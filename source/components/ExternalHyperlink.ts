export class ExternalHyperlink extends HTMLAnchorElement {
	constructor() {
		super();
		this.target = "_blank";
		this.rel = "noopener noreferrer nofollow";
	}
}

window.customElements.define("x-external-hyperlink", ExternalHyperlink, { extends: "a" });