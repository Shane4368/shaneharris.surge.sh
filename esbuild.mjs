import { copyFileSync, existsSync, mkdirSync } from "node:fs";
import { buildSync } from "esbuild";

buildSync({
	entryPoints: ["source/index.ts"],
	outfile: "static/index.js",
	format: "esm",
	target: "es2022",
	bundle: true
});

copyFileSync("./source/index.html", "./static/index.html");

if (!existsSync("./static/assets")) mkdirSync("./static/assets");

copyFileSync("./assets/shane-harris.jpg", "./static/assets/shane-harris.jpg");